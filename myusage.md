Personally I use the following in chain:  
  
\v4a paid irs\Apple iPod Equalizer ((44.48k Z.H-Edition)) v4\48k\Apple iPod ((48k Z-Edition)) 08.Flat..wav  
And  
\KIRILL ED\Sony Hi-Fi & Technics Preamp & Amp Default.wav  
But sometimes, I add  
\spacial irs\EWQL Spaces - IR Library\2 CHURCHES\Berlin Church TS\Berlin Church A TS FR 4.8s L.wav  
to the chain when I want to listen to death metal LUL