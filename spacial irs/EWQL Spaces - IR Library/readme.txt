﻿QUANTUM LEAP SPACES from EASTWEST is a 24-bit, true stereo convolution reverb that sets a new standard by utilizing new impulse recording techniques developed over the past seven years.

QUANTUM LEAP SPACES includes a very hi-resolution, true stereo engine that focuses on delivering the very best sound at a very low CPU load. Most impulses are in 8 channel format, so surround mixes can be created. In the end, what makes QUANTUM LEAP SPACES unique is that every impulse is special. There is no wading through endless folders of cryptically labelled identical sounding impulses. In fact, QUANTUM LEAP SPACES takes the mystery out of choosing reverbs by suppling suitable recommendations for every instrument and style.

 QUANTUM LEAP SPACES has custom impulses taken over the past seven years from around the globe, with a focus on the Western United States. QUANTUM LEAP SPACES includes concert halls (including the EWQLSO Hall), churches, cathedrals, caverns, rock studios (including EastWest Studios), soundstages, forests, a swimming hall, parking garages, water tanks, a tunnel, vintage plates and custom digital reverb preset impulses.

* Produced and Engineered By Nick Phoenix
* Highest resolution, most useful, reverbs available
* 24-bit True Stereo, with True Surround capability
* ATC, Neumann, Telefunken, Neve, Meitner Signal Path
* Film Score, Rock, Pop, Jazz, Classical, Esoteric and Dreamy Reverbs
* Low CPU usage, with cleanest signal path possible



This IR library was ripped from EASTWEST QUANTUM LEAP SPACES and can be used with any true stereo convolution reverb. No more buggy plugin, no more trashy PACE/iLok, yes more performance, yes more tweaking.

SPACES is particularly known for containing EWQLSO Hall / Northwest Orchestral Hall true stereo preset, the hall that was used for recording EWQL Symphonic Orchestra. The preset can be used to blend with that library seamlessly.

SPACES plugin may be a clumsy DRM makeweight but the library itself is brilliant, so if you make money with it or use it a lot, consider buying the original product and putting it on a shelf.

All impulse responses are stereo/24-bit/48kHz WAV files.

Technical details and preset information are included in EWQL Spaces IR library.html
